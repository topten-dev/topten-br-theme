var gulp           = require('gulp'), // Gulp
    sass           = require('gulp-sass'), // SASS,
    changed        = require('gulp-changed'), // detect changes
    sourcemaps     = require('gulp-sourcemaps'), // sourcemaps
    autoprefixer   = require('gulp-autoprefixer'); // Add the desired vendor prefixes and remove unnecessary in SASS-files

gulp.task('build-theme', function() {
    return gulp.src('./assets/lib/scss/br-theme.scss')
      .pipe(changed('./assets/lib/css/'))
      .pipe(sourcemaps.init())
      .pipe(sass({outputStyle:'compressed'}))
      .pipe(autoprefixer(['last 3 versions', '> 1%'], { cascade: true }))
      .pipe(sourcemaps.write())
      .pipe(gulp.dest('./assets/lib/css/'))
  });

  gulp.task('watch', function() {
    gulp.watch('./assets/lib/scss/br-theme.scss', ['build-theme']);
  });

  gulp.task('default', ['watch', 'build-theme']);