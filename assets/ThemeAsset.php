<?php

namespace Topten\BrazilTheme;

use common\assets\Html5shiv;
use common\assets\AdminLte;
use common\assets\LazyLoad;
use frontend\assets\StandardAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

class ThemeAsset extends AssetBundle
{
    public $sourcePath = '@vendor/topten-dev/topten-br-theme/assets/lib';
    public $css = [
        '//fonts.googleapis.com/css?family=Comfortaa:300,400,700',
        'css/br-theme.css',
    ];
    public $js = [
    ];
    public $depends = [
        YiiAsset::class,
        AdminLte::class,
        Html5shiv::class,
        LazyLoad::class,
        StandardAsset::class,
    ];
}