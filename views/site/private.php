<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->context->layout = "@vendor/topten-dev/topten-br-theme/views/layouts/no-container";

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */
$this->params["no-header"] = true;
$this->title = 'Topten';

$customSection = env('PRIVATE_CUSTOM_URL', 'private');
$bundle = \Topten\BrazilTheme\ThemeAsset::register($this);
?>

<div class="site-private container-fluid">
    <div class="row">
        <div class="col-xs-12 no-padding margin-top-35">
            <?php echo \common\widgets\DbCarousel::widget([
                'key'=>'index-'.\Yii::$app->language,
                'assetManager' => Yii::$app->getAssetManager(),
                'options' => [
                    'class' => 'slide floating-indicators', // enables slide effect
                ],
                'controls' => false,
                'showIndicators' => true,
                'width' => 1920,
                'height' => 546,
            ]) ?>
        </div>
    </div>

    <div class="row frontpage-row equal">
        <div class="col-md-4 bg-orange align-middle justify-right"><div class="row-wrapper"><h3 class="row-title text-blue">principais<br>categorias</h3></div></div>
        <div class="col-md-8 bg-orange category-padding>
            <div class="row align-middle">
                <div class="col-md-4 align-middle">
                    <?= Html::img($bundle->baseUrl.'/img/washer-icon.png', ['class' => 'category-icon']) ?>
                    <ul class="category-list">
                        <li><?= Html::a('lava e seca', ["$customSection/products/wash_and_dry"]) ?></li>
                        <li><?= Html::a('tanquinho', ["$customSection/wm_semiautomatic"]) ?></li>
                        <li><?= Html::a('lavadora automatica', ["$customSection/products/wm_automatic"]) ?></li>
                    </ul>
                </div>
                <div class="col-md-4 align-middle">
                    <?= Html::img($bundle->baseUrl.'/img/fridge-icon.png', ['class' => 'category-icon']) ?>
                    <ul class="category-list">
                        <li><?= Html::a('freezer', ["$customSection/products/freezers"]) ?></li>
                        <li><?= Html::a('refrigerador', ["$customSection/refrigerators"]) ?></li>
                    </ul>
                </div>
                <div class="col-md-4 align-middle">
                    <?= Html::img($bundle->baseUrl.'/img/tv-icon.png', ['class' => 'category-icon']) ?>
                    <ul class="category-list">
                        <li><?= Html::a('tvs', ["$customSection/products/tvs"]) ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row frontpage-row equal ">
        <div class="col-md-4 bg-blue align-middle justify-right">
            <div class="row-wrapper">
                <?= Html::img($bundle->baseUrl.'/img/logo_vertical.png', ['class' => 'row-title-img']) ?>
                <h3 class="row-title text-white">o<br>que<br>é?</h3>
            </div>
        </div>

        <div class="col-md-8 bg-white align-middle justify-right">
            <p class="text-blue font-larger">A topten.eco.br é uma plataforma online inovadora que analisa a eficiência energética de produtos eletrodomésticos e eletroeletrônicos, buscando auxiliar o consumidor a realizar uma compra com maior consciência ambiental. <a href="/<?= $customSection?>/page/quem-somos">Saiba mais</a></p>
        </div>
    </div>

    <div class="row frontpage-row equal">
        <div class="col-md-4 bg-orange align-middle justify-right"><div class="row-wrapper"><h3 class="row-title text-blue">critério<br>de seleção</h3></div></div>
        <div class="col-md-8 no-padding bg-grey align-middle">
            <?= Html::img($bundle->baseUrl.'/img/selection_criteria.jpg', ['class' => 'float-left img-responsive', 'style'=>'height: 200px;']) ?>
            <p class="text-blue font-larger float-left padding-left-25 padding-right-25">Apresentamos os melhores produtos de acordo com critérios claros e específicos. Todas as informações sobre cálculos e critérios de seleção de produtos estão disponíveis aqui. <a href="/<?= $customSection?>/selection-criteria/index">Saiba mais</a></p>
        </div>
    </div>

    <div class="row frontpage-row equal">
        <div class="col-md-4 bg-white align-middle justify-right"><div class="row-wrapper"><h3 class="row-title text-blue">dicas sobre<br>produtos</h3></div></div>
        <div class="col-md-8 no-padding bg-white">
            <ul class="list-inline no-margin">
                <li class="no-padding">
                    <?= Html::a(Html::img($bundle->baseUrl.'/img/tvs.jpeg', ['class' => 'img-responsive']), ["$customSection/adviser/como-escolher-televisor"], ['title' => 'Dicas sobre televisores', 'data' => ['toggle' => 'tooltip', 'placement' => 'auto']]) ?>
                </li>
                <li class="no-padding">
                    <?= Html::a(Html::img($bundle->baseUrl.'/img/fridge.jpeg', ['class' => 'img-responsive']), ["$customSection/adviser/como-escolher-o-seu-refrigerador"], ['title' => 'Dicas sobre refrigeradores', 'data' => ['toggle' => 'tooltip', 'placement' => 'auto']]) ?>
                </li>
                <li class="no-padding">
                    <?= Html::a(Html::img($bundle->baseUrl.'/img/washers.jpeg', ['class' => 'img-responsive']), ["$customSection/adviser/como-escolher-a-sua-lavadora"], ['title' => 'Dicas sobre lavadoras de roupas', 'data' => ['toggle' => 'tooltip', 'placement' => 'auto']]) ?>
                </li>
                <li><a href="/<?= $customSection?>/adviser/index" class="text-blue" ><i class="fa fa-chevron-right fa-4x"></i></a></li>
            </ul>
        </div>
    </div>

    <div class="row frontpage-row equal">
        <div class="col-md-4 bg-blue align-middle justify-right"><div class="row-wrapper"><h3 class="row-title text-orange">notícias</h3></div></div>
        <div class="col-md-8 bg-grey">
            <ul class="list-unstyled">
                <?php foreach($articles as $article): ?>

                    <?php $articleTranslation = \common\models\NodeContent::find()->where(['node_id' => $article->id, 'language' => \Yii::$app->language])->limit(1)->one(); ?>

                    <li class="margin-bottom-10"><?php echo Html::a($articleTranslation->title, [$customSection.'/article/'.$articleTranslation->slug], ['class' => 'text-blue']) ?></li>
                <?php endforeach; ?>
                <li><?php echo Html::a('Veja todas as notícias', [$customSection.'/article/'.$articleTranslation->slug]) ?></li>
            </ul>
        </div>
    </div>

    <div class="row frontpage-row equal margin-bottom-0">
        <div class="col-md-4 bg-white align-middle justify-right"><div class="row-wrapper"><h3 class="row-title text-orange">publicações</h3></div></div>
        <div class="col-md-8 no-padding bg-white">
            <ul class="list-inline">
            <?php foreach (\common\models\Node::find()->documentation()->published()->orderBy('created_at DESC')->limit(3)->all() as $documentation): ?>
                <li><?= Html::img(Yii::$app->glide->createSignedUrl([
                        'glide/index',
                        'path' => $documentation->thumbnail_path,
                        'h' => 200,
                        'fit' => 'contain',
                    ], true), ['alt' => "publicaçõe"]); ?></li>
            <?php endforeach; ?>
                <li><a href="/<?= $customSection?>/documentation/index" class="text-orange" ><i class="fa fa-chevron-right fa-4x"></i></a></li>
            </ul>
        </div>
    </div>
</div>