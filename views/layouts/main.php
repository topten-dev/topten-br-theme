<?php
/* @var $this \yii\web\View */
use yii\helpers\ArrayHelper;
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;

/* @var $content string */
$this->beginContent('@vendor/topten-dev/topten-br-theme/views/layouts/base.php')
?>
<div class="container">
    <section class="content">
        <?php echo $content ?>
    </section>
</div>
<?php $this->endContent() ?>
