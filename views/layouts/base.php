<?php

use common\widgets\DbNav;
use yii\helpers\ArrayHelper;
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;

$bundle = \Topten\BrazilTheme\ThemeAsset::register($this);

// setup header and subtitle
$headerEncode = isset($this->params["header"]["encode"]) ? $this->params["header"]["encode"] : true;
$headerTitle = Html::encode($this->title);
$headerSubtitle = '';

if (isset($this->params["header"]["title"])) {
    $headerTitle = $headerEncode ? Html::encode($this->params["header"]["title"]) : $this->params["header"]["title"];
}
if (isset($this->params["header"]["subtitle"])) {
    $headerSubtitle = $headerEncode ? Html::encode($this->params["header"]["subtitle"]) : $this->params["header"]["subtitle"];
}

// setup breadcrumbs
$breadcrumbs = [];
if (isset($this->params['breadcrumbs'])) {
    $breadcrumbs = $this->params['breadcrumbs'];
} else {
    $breadcrumbs[] = ['label' => $headerTitle];
}

/* @var $this \yii\web\View */
/* @var $content string */

$this->beginContent('@vendor/topten-dev/topten-br-theme/views/layouts/_clear.php')
?>
<div class="wrapper with-overflow">
    <header class="main-header">
        <!-- Mobile Navigation Menu -->
        <?= $this->render('_mobile-navbar') ?>
        <!-- /Mobile Navigation Menu -->

        <!-- Desktop Navigation Menu -->
        <?= $this->render('_desktop-navbar') ?>
        <!-- /Desktop Navigation Menu -->
    </header>

    <div class="content-wrapper add-margin-affix">
        <div class="container">
            <?php if(Yii::$app->session->hasFlash('alert')):?>
                <?php echo \yii\bootstrap\Alert::widget([
                    'body'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'body'),
                    'options'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'options'),
                ])?>
            <?php endif; ?>

            <?php if (!isset($this->params["no-header"])): ?>
                <div class="row equal">
                    <div class="<?= empty($this->params['partner_link'])? "col-sm-12": "col-sm-8" ?>">
                        <section class="content-header">
                            <h1>
                                <?php echo $headerTitle ?>
                                <small><?php echo $headerSubtitle ?></small>
                            </h1>
                            <?php if (!empty($this->params['links'])): ?>
                                <div class="btn-group btn-group-sm margin-bottom-10" role="group">
                                    <?php foreach ($this->params['links'] as $link): ?>
                                        <?= Html::a($link['label'], $link['url'], empty($link['options'])? ['class' => 'btn btn-topten'] : $link['options']) ?>
                                    <?php endforeach; ?>
                                </div>
                            <?php else: ?>
                                <?php echo Breadcrumbs::widget([
                                    'homeLink' => [
                                        'label' => '<i class="fa fa-home"></i> '. Yii::t('frontend', 'Home'),
                                        'url' => '/'
                                    ],
                                    'links' => $breadcrumbs,
                                    'encodeLabels' => false,
                                ]) ?>
                            <?php endif; ?>
                        </section>
                    </div>

                    <?php if (!empty($this->params['partner_link'])): ?>
                    <div class="col-sm-4">
                        <?= Html::a($this->params['partner_link']['label'], $this->params['partner_link']['url'], empty($this->params['partner_link']['options'])? ['class' => 'btn btn-topten btn-partner'] : $this->params['partner_link']['options']) ?>
                    </div>
                    <?php endif; ?>
                </div>

            <?php endif; ?>
        </div>

        <?php echo $content; ?>

        <?php if (Yii::$app->controller->id == 'product') {
            echo '<div class="container"><blockquote class="legal-text"><i class="fa fa-info-circle"></i> A topten.eco.br não é uma plataforma de venda online. Todo detalhe adicional do produto, incluindo formas de pagamentos, despesas e taxas de compra, prazos de entrega, condições de troca e devolução, condições de preços ou promoções, contratos de venda e nota fiscal, canais de contato e central de relacionamento são de responsabilidade do site que opera o processo de venda do produto. As imagens disponíveis na topten.eco.br são meramente ilustrativas, fornecidas pelos próprios fabricantes ou proveniente do site dos fabricantes.
            A topten.eco.br não exige nenhum dado do cliente referente à compra de produtos, portanto, não se aplicando a Lei 12.741/2012 de certificado digital. Para mais informações, acesse o Código de Defesa do Consumidor <a href="http://www.planalto.gov.br/ccivil_03/Leis/L8078.htm" target="_blank">aqui</a>.</blockquote></div>';
        } ?>
    </div>

    <!-- partners and copyright info -->
    <footer class="partners-footer">
        <div class="container-fluid">
            <div class="row frontpage-row equal no-margin-bottom">
                <div class="col-md-4 bg-orange align-middle"><div class="row-wrapper"><h3 class="row-title text-blue">parceiros</h3></div></div>
                <div class="col-md-8 bg-grey">
                    <ul class="partner-logos">
                        <li>
                            <a href="http://www.worldwildlife.org/" target="_blank">
                                <img src="<?= $bundle->baseUrl.'/img/wwf_grey.jpg' ?>" class="img-responsive" alt="WWF" style="height: 70px;">
                            </a>
                        </li>
                        <li>
                            <a href="http://bsdconsulting.com/" target="_blank">
                                <img src="<?= $bundle->baseUrl.'/img/BSD-logo.png' ?>" class="img-responsive" alt="BSD Consulting" style="height: 80px;">
                            </a>
                        </li>
                        <li>
                            <a href="https://www.topten.eu/" target="_blank">
                                <img src="<?= $bundle->baseUrl.'/img/topten.png' ?>" class="img-responsive" alt="Topten" style="height: 35px;">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <footer class="general-footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <strong>Software:</strong> Topten International Group &copy; / <a href="mailto:victor@vgr.cl" class="text-orange">VGR SpA</a> <?= date('Y')?> &bullet;
                    <strong><?= Yii::t('frontend', 'Content') ?>:</strong> BSD Consulting &copy; <?= date('Y')?>
                </div>
            </div>
        </div>
    </footer>
</div>

<?php $this->endContent() ?>
