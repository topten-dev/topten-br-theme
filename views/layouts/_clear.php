<?php
use yii\helpers\Html;
/* @var $this \yii\web\View */
/* @var $content string */

\frontend\assets\StandardAsset::register($this);
\Topten\BrazilTheme\ThemeAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>">
<head>
    <meta charset="<?php echo Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php if(YII_ENV_DEV): ?>
        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta name="robots" content="none" />
    <?php else: ?>
        <meta http-equiv="expires" content="<?= date(\DateTime::COOKIE, strtotime('tomorrow midnight')) ?>" />
        <meta http-equiv="cache-control" content="max-age=86400" />
        <meta name="robots" content="all" />
    <?php endif; ?>
    <meta name="MobileOptimized" content="width" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta http-equiv="cleartype" content="on" />

    <?php if (empty(Yii::$app->params['seo_title'])): ?>
        <title><?php echo Html::encode($this->title) ?></title>
    <?php else: ?>
        <title><?php echo Html::encode(Yii::$app->params['seo_title']) ?></title>
    <?php endif; ?>

    <?php if (!empty(Yii::$app->params['seo_description'])): ?>
        <meta name="description" content="<?php echo Html::encode(Yii::$app->params['seo_description']) ?>">
    <?php endif; ?>

    <?php if (!empty(Yii::$app->params['seo_tags'])): ?>
        <meta name="keywords" content="<?php echo Html::encode(Yii::$app->params['seo_tags']) ?>">
    <?php endif; ?>

    <?php $this->head() ?>
    <?php echo Html::csrfMetaTags() ?>

</head>
<body class="layout-top-nav skin-green-light">
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', "<?= env('GOOGLE_ANALYTICS') ?>", 'auto');
        ga('set', 'anonymizeIp', true);
        ga('send', 'pageview');
    </script>
<?php $this->beginBody() ?>
    <?php echo $content ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
