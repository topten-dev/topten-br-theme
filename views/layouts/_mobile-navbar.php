<?php
use common\widgets\NavTopten;
use yii\bootstrap\Nav;
use yii\helpers\Html;

$bundle = \Topten\BrazilTheme\ThemeAsset::register($this);
?>

<nav class="navbar navbar-default navbar-fixed-top navbar-mobile-topten hidden-sm hidden-md hidden-lg">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mobile-menu" aria-expanded="false">
                <span class="sr-only">Topten Menu</span>
                <span class="icon-bar top-bar"></span>
                <span class="icon-bar middle-bar"></span>
                <span class="icon-bar bottom-bar"></span>
            </button>

            <?php echo  Html::a(Html::img($bundle->baseUrl.'/img/logo_small.png', ['class' => 'img-responsive']), '/'.getCustomSection(Yii::$app->params['section']), ['class' => 'navbar-brand']) ?>


            <form action="/search" class="search-input-form" role="search">
                <input type="text" name="q" placeholder="<?= Yii::t('frontend', 'Search...') ?>">
            </form>

        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="mobile-menu">
            <?php echo Nav::widget([
                'options' => ['class' => 'navbar-nav nav-pills nav-fill links-menu'],
                'items' => [
                    [
                        'label' => '<i class="fa fa-facebook-official"></i></a>',
                        'url' => 'https://www.facebook.com/topten.eco.br/',
                        'linkOptions' => ['target' => '_blank'],
                        'encode' => false,
                    ],
                    [
                        'label' => '<i class="fa fa-instagram"></i></a>',
                        'url' => 'https://instagram.com/topten.eco.br/',
                        'linkOptions' => ['target' => '_blank'],
                        'encode' => false,
                    ],
                    [
                        'label' => Yii::t('frontend', 'More'),
                        'url' => '#',
                        'encodeLabels' => false,
                        'items' => [
                            [
                                'label' => Yii::t('frontend', '&laquo; Back'),
                                'url' => '#',
                                'options' => ['class' => 'back-button']
                            ],
                            [
                                'label' => Yii::t('frontend', 'About Us'),
                                'url' => [Yii::$app->params['section'].'/page/quem-somos'],
                            ],
                            [
                                'label' => Yii::t('frontend', 'Partner'),
                                'url' => [Yii::$app->params['section'].'/page/parceiros'],
                            ],
                            [
                                'label' => Yii::t('frontend', 'Contact'),
                                'url' => ['site/contact'],
                            ],
                        ],
                    ],
                ],
            ]); ?>
            <?php echo NavTopten::widget([
                'options' => ['class' => 'navbar-nav products-menu'],
                'encodeLabels' => false,
            ]); ?>
        </div><!-- /.navbar-collapse -->


    </div><!-- /.container-fluid -->
</nav>
