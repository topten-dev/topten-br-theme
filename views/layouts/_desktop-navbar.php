<?php
use common\widgets\NavTopten;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use yii\helpers\Html;
$bundle = \Topten\BrazilTheme\ThemeAsset::register($this);
$appSection = Yii::$app->params['section'];
?>

<?php NavBar::begin([
    'id' => 'navtopten',
    'brandLabel' => false,
    'renderInnerContainer' => false,
    'options' => [
        'class' => 'navbar-inverse navbar-fixed-top navbar-topten hidden-xs',
    ],
]); ?>
    <div class="container">
        <div class="navbar-header">
            <?= Html::a(Html::img($bundle->baseUrl.'/img/logo_small.png', ['class' => 'img-responsive']), '/'.getCustomSection($appSection), ['class' => 'navbar-brand']) ?>
        </div>

        <!-- Nav Menu -->
        <?php echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                [
                    'label' => Yii::t('frontend', 'About Us'),
                    'url' => [getCustomSection($appSection).'/page/quem-somos'],
                ],
                [
                    'label' => Yii::t('frontend', 'Partner'),
                    'url' => [getCustomSection($appSection).'/page/parceiros'],
                ],
                [
                    'label' => Yii::t('frontend', 'Contact'),
                    'url' => ['site/contact'],
                ],
                '<form action="/search" class="navbar-form topten-navbar-form" role="search">
                    <div class="input-group">
                        <input type="text" name="q" class="form-control" placeholder="'.Yii::t('frontend', 'Search...').'">
                        <span class="input-group-btn" title="Submit"><button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button></span>
                    </div>
                </form>',
                [
                    'label' => '<i class="fa fa-facebook-official"></i></a>',
                    'url' => 'https://www.facebook.com/topten.eco.br/',
                    'linkOptions' => ['target' => '_blank', 'class' => 'text-blue'],
                    'options' => ['class' => 'social-icon'],
                    'encode' => false,
                ],
                [
                    'label' => '<i class="fa fa-instagram"></i></a>',
                    'url' => 'https://instagram.com/topten.eco.br/',
                    'linkOptions' => ['target' => '_blank', 'class' => 'text-blue'],
                    'options' => ['class' => 'social-icon'],
                    'encode' => false,
                ],
            ],
        ]); ?>
    </div>



    <div class="nav-topten-wrapper">
        <div class="container">
            <!-- Products Menu -->
            <?php echo NavTopten::widget([
                'options' => ['class' => 'navbar-nav products-menu'],
                'encodeLabels' => false,
            ]); ?>
        </div>
    </div>


<?php NavBar::end(); ?>
