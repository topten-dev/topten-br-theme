# Topten Brazil Theme

This repository contains the official Topten Brazil Theme used for the Topten CMS.

## Installation

### Obtaining the files

The preferred way to install the Topten Brazil Theme is through `composer`. To start run the following command inside your Topten CMS project:

```
composer require --prefer-dist topten-dev/topten-br-theme "^1.0"
```

Make sure to run `composer update` if the `composer require` command doesn't download the files.

### Setup the mapping

Once the extension is installed, you have to setup the mapping of the views for the frontend application of the Topten CMS. To this, please edit the file `frontend/web/config.php`, uncomment the block that starts with the comment `// uncomment the following blocks for custom themes`. Then modify the path of the vendor section so it looks like this:

```
'@frontend/views' => '@vendor/topten-dev/topten-br-theme/views',
```

Once the change is saved, the new theme will take precedence over the default theme bundled with the Topten CMS.

**You are done!**

## Development

### Install Dependencies

The prefered way to develop on the Topten Brazil Theme is through `yarn`. To start, please make sure you have installed the `yarn` command, then run:

```
yarn install
```

Or, if you don't have ´yarn´, or if you prefer to just use `npm`, please run:

```
npm install
```

Once the download of packages has finished, you can start developing on the Topten Brazil Theme.

### GULP commands

The Topten Brazil Theme uses SASS for the stylesheet pre-processor, and GULP to automate the build of the production-ready files.

This project comes with the following commands:

```
gulp build-theme
```

Which processes the source SASS file and outputs a CSS file in its corresponding folder.

```
gulp watch
```

Which detects any change done to the source file, and automatically builds the CSS file.

```
gulp
```

Defaults to run the `build-theme` task and then runs the `watch` task.